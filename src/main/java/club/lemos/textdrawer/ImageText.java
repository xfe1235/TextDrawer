package club.lemos.textdrawer;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class ImageText {

    private Integer x;

    private Integer y;

    private String text;

    private boolean nextLine;

    public static final String SPACE_CHAR = " ";

    public List<TextTip> tips = new ArrayList<>();

    public ImageText() {
    }

    public ImageText(String content) {
        this.text = content;
    }

    public static ImageText nextLine() {
        ImageText imageText = new ImageText();
        imageText.setNextLine(true);
        return imageText;
    }

    public static ImageText text(String str) {
        return new ImageText(str);
    }

    public ImageText padding(int v) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < v; i++) {
            sb.append(SPACE_CHAR);
        }
        sb.append(this.getText());
        for (int i = 0; i < v; i++) {
            sb.append(SPACE_CHAR);
        }
        this.setText(sb.toString());
        return this;
    }

    public ImageText positionX(int x) {
        this.x = x;
        return this;
    }

    public ImageText positionY(int y) {
        this.y = y;
        return this;
    }

    public ImageText position(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public ImageText tip(TextTip tip) {
        if (tip.getRange() == null) {
            tip.setRange(new TextRange(0, text.length()));
        }
        this.tips.add(tip);
        return this;
    }

    public ImageText underline() {
        TextTip textTip = new TextTip();
        textTip.setUnderline(true);
        //下划线首尾留空
        this.text = SPACE_CHAR + this.text + SPACE_CHAR;
        textTip.setRange(new TextRange(1, text.length() - 1));
        this.tip(textTip);
        return this;
    }

    public ImageText tips(List<TextTip> tips) {
        this.tips = tips;
        return this;
    }

}