package club.lemos.textdrawer;


import lombok.Data;

/**
 * 文字范围
 */
@Data
public class TextRange {
    /*
     开始的位置。从 0 开始。
     */
    private int start;
    /*
    结束的位置
     */
    private int end;

    public TextRange(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
