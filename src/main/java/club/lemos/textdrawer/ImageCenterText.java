package club.lemos.textdrawer;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ImageCenterText {

    private Integer x1;

    private Integer x2;

    private Integer y;

    private String text;

    public List<TextTip> tips = new ArrayList<>();

    public ImageCenterText(String content) {
        this.text = content;
    }

    public static ImageCenterText text(String str) {
        return new ImageCenterText(str);
    }

    public ImageCenterText position(int x1, int x2, int y) {
        this.x1 = x1;
        this.x2 = x2;
        this.y = y;
        return this;
    }

    public ImageCenterText tip(TextTip tip) {
        this.tips.add(tip);
        return this;
    }

    public ImageCenterText tips(List<TextTip> tips) {
        this.tips = tips;
        return this;
    }

}
