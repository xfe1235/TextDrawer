package club.lemos.textdrawer;


import lombok.Data;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Map;


@Data
public class TextTip {
    private Color color;
    private Font font;
    private TextRange range;

    private boolean underline;

    private Map<TextAttribute, Object> attributes;

    public TextTip() {
    }

    public TextTip(TextRange range) {
        this.range = range;
    }

    public TextTip color(Color color) {
        this.color = color;
        return this;
    }

    public TextTip font(Font font) {
        this.font = font;
        return this;
    }

    public TextTip attributes(Map<TextAttribute, Object> attributes) {
        this.attributes = attributes;
        return this;
    }

    public TextTip underline() {
        this.underline = true;
        return this;
    }

    public boolean isHit(int i) {
        TextRange range = this.getRange();
        if (range == null) {
            return true;
        }
        return i >= range.getStart() && i < range.getEnd();
    }

    public static TextTip range() {
        return new TextTip();
    }

    public static TextTip range(int start, int end) {
        return new TextTip(new TextRange(start, end));
    }


}
