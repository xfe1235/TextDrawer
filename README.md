# TextDrawer

一个用于动态生成“证书”类图片的小工具

示例：

```java
import club.lemos.textdrawer.TextDrawer;

public class App {

    public static void main(String[] args) throws IOException {
        BufferedImage bgImg = new BufferedImage(2400, 1400, BufferedImage.TYPE_INT_RGB);
        TextDrawer
                .init(bgImg, 50, 50, new Font(null, Font.PLAIN, 150), 306)
                .defaultColor(new Color(73, 73, 73))
                .padding(10, 10)
                .draw(ImageCenterText.text("你好，")
                        .position(0, 500, 400)
                        .tip(TextTip.range(0, 1).color(Color.ORANGE)))
                .draw(ImageText.text("我的爱人").tip(TextTip.range().color(Color.RED)))
                .draw(ImageText.text("眼睛是心灵的窗户").padding(4).underline())
                .draw(ImageText.nextLine())
                .draw(true, ImageText.text("明天再见").positionX(200))
                .show();
//                .output(new File("C:\\tmp\\abc", "d.jpg"));
    }

}
```